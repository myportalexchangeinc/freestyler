package com.jprog.sample.fresstyle;

import android.os.AsyncTask;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.Socket;
import java.net.SocketException;
import java.util.Enumeration;

/**
 * Created by window on 7/22/2016.
 */
public class SendColor {


    Socket socket;
    String setIp;
    String input;


    public SendColor(Socket socket, String setIp, String input) {

        this.socket = socket;
        this.setIp = setIp;
        this.input = input;


    }


    public void callFree() {
        Cyan task = new Cyan();
        Magenta task1 = new Magenta();
        Yellow task2 = new Yellow();
        task.execute(new String[]{"NO"});
        task1.execute(new String[]{"NO"});
        task2.execute(new String[]{"NO"});
    }


    private class Cyan extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            final int SocketServerPORT = 3332;
            try {

                socket = new Socket(setIp, SocketServerPORT);

                DataInputStream DIStream = new DataInputStream(socket.getInputStream());
                DataOutputStream DOStream = new DataOutputStream(socket.getOutputStream());


                DOStream.write(input.getBytes(), 0, input.getBytes().length);
                DOStream.flush();

                //  String text = DIStream.readLine();

                DOStream.close();
                DIStream.close();


                socket.close();


            } catch (IOException e) {

            }


            return null;
        }


        @Override
        protected void onPostExecute(String result) {


        }

    }



    private class Magenta extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            final int SocketServerPORT = 3332;
            try {

                socket = new Socket(setIp, SocketServerPORT);

                DataInputStream DIStream = new DataInputStream(socket.getInputStream());
                DataOutputStream DOStream = new DataOutputStream(socket.getOutputStream());


                DOStream.write(input.getBytes(), 0, input.getBytes().length);
                DOStream.flush();

                //  String text = DIStream.readLine();

                DOStream.close();
                DIStream.close();


                socket.close();


            } catch (IOException e) {

            }


            return null;
        }


        @Override
        protected void onPostExecute(String result) {


        }

    }



    private class Yellow extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            final int SocketServerPORT = 3332;
            try {

                socket = new Socket(setIp, SocketServerPORT);

                DataInputStream DIStream = new DataInputStream(socket.getInputStream());
                DataOutputStream DOStream = new DataOutputStream(socket.getOutputStream());


                DOStream.write(input.getBytes(), 0, input.getBytes().length);
                DOStream.flush();

                //  String text = DIStream.readLine();

                DOStream.close();
                DIStream.close();


                socket.close();


            } catch (IOException e) {

            }


            return null;
        }


        @Override
        protected void onPostExecute(String result) {


        }

    }

    private String getIpAddress() {
        String ip = "";
        try {
            Enumeration<NetworkInterface> enumNetworkInterfaces = NetworkInterface
                    .getNetworkInterfaces();
            while (enumNetworkInterfaces.hasMoreElements()) {
                NetworkInterface networkInterface = enumNetworkInterfaces
                        .nextElement();
                Enumeration<InetAddress> enumInetAddress = networkInterface
                        .getInetAddresses();
                while (enumInetAddress.hasMoreElements()) {
                    InetAddress inetAddress = enumInetAddress.nextElement();

                    if (inetAddress.isSiteLocalAddress()) {
                        ip += "SiteLocalAddress: "
                                + inetAddress.getHostAddress() + "\n";
                    }

                }

            }

        } catch (SocketException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            ip += "Something Wrong! " + e.toString() + "\n";
        }

        return ip;
    }
}
