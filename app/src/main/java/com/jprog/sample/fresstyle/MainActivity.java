package com.jprog.sample.fresstyle;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends Activity implements View.OnClickListener {

    Context context;
    Socket socket = null;

    DataInputStream dataInputStream = null;
    DataOutputStream dataOutputStream = null;

    //  Thread socketServerThread = new Thread(new SocketServerThread());
    String input;
    EditText getIp, ed_Code;
    String setIp, newCode;
    Button btn_sendColor, activate, btn_Code;
    TextView tv_cyan, tv_magenta, tv_yellow, tv_isMagenta, tv_isYellow, tv_isCyan;
    SeekBar sb_cyan, sb_magenta, sb_yellow;

    String getCyan, getMagenta, getYellow;
    private float mDownX;
    private float mDownY;
    private final float SCROLL_THRESHOLD = 10;
    private boolean isOnClick;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getIp = (EditText) findViewById(R.id.getIp);
        ed_Code = (EditText) findViewById(R.id.ed_Code);
        activate = (Button) findViewById(R.id.activate);
        btn_sendColor = (Button) findViewById(R.id.btn_sendColor);
        btn_Code = (Button) findViewById(R.id.btn_Code);
        sb_cyan = (SeekBar) findViewById(R.id.sb_cyan);
        sb_magenta = (SeekBar) findViewById(R.id.sb_magenta);
        sb_yellow = (SeekBar) findViewById(R.id.sb_yellow);
        tv_cyan = (TextView) findViewById(R.id.tv_cyan);
        tv_magenta = (TextView) findViewById(R.id.tv_magenta);
        tv_yellow = (TextView) findViewById(R.id.tv_yellow);

        tv_isCyan = (TextView) findViewById(R.id.tv_isCyan);


//
//      //  getIp.setVisibility(View.GONE);
//        ed_Code.setVisibility(View.GONE);
//      //  activate.setVisibility(View.GONE);
//        btn_sendColor.setVisibility(View.GONE);
//        btn_Code.setVisibility(View.GONE);
//        sb_cyan.setVisibility(View.GONE);
//        sb_magenta.setVisibility(View.GONE);
//        sb_yellow.setVisibility(View.GONE);
//        tv_cyan.setVisibility(View.GONE);
//        tv_magenta.setVisibility(View.GONE);
//        tv_yellow.setVisibility(View.GONE);
//        tv_isCyan.setVisibility(View.GONE);

        context = this;
        btn_sendColor.setOnClickListener(this);
        activate.setOnClickListener(this);
        btn_Code.setOnClickListener(this);

        sb_cyan.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                String addedZero = "";
                if (progress < 10) {
                    addedZero = "00" + progress;
                } else if (progress < 99) {
                    addedZero = "0" + progress;
                } else
                    addedZero = progress + "";

                getCyan = "FSOC130" + addedZero;
                Log.d("getCyan", getCyan + "");
                tv_cyan.setText(getCyan);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        sb_magenta.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                String addedZero = "";
                if (progress < 10) {
                    addedZero = "00" + progress;
                } else if (progress < 99) {
                    addedZero = "0" + progress;
                } else
                    addedZero = progress + "";


                getMagenta = "FSOC131" + addedZero;
                Log.d("getMagenta", getMagenta + "");
                tv_magenta.setText(getMagenta);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        sb_yellow.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                String addedZero = "";
                if (progress < 10) {
                    addedZero = "00" + progress;
                } else if (progress < 99) {
                    addedZero = "0" + progress;
                } else
                    addedZero = progress + "";

                getYellow = "FSOC132" + addedZero;
                Log.d("yellow", getYellow + "");
                tv_yellow.setText(getYellow);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.activate:
                //  loadContent() ;
                Log.d("TAPPED", "tapped");
                newCode = "FS";
                // setIp = "192.168.6.107";
                setIp = getIp.getText().toString().trim();
                //  socketServerThread.start();
                FreesStuff task = new FreesStuff();
                task.execute(new String[]{"NO"});

                break;

            case R.id.btn_sendColor:
                Cyan task0 = new Cyan();
                task0.execute(new String[]{"NO"});
                Magenta task1 = new Magenta();
                task1.execute(new String[]{"NO"});
                Yellow task2 = new Yellow();
                task2.execute(new String[]{"NO"});
                break;


            case R.id.btn_Code:
                Log.d("TAPPED", "tapped");
                newCode = "FSOC" + ed_Code.getText().toString().trim();
                //  socketServerThread.start();
                FreesStuff task4 = new FreesStuff();
                task4.execute(new String[]{"NO"});
                break;
        }

    }


    private class FreesStuff extends AsyncTask<String, Void, String> {
        String res = "";

        @Override
        protected String doInBackground(String... urls) {
            final int SocketServerPORT = 3332;

            try {

                socket = new Socket(setIp, SocketServerPORT);

                DataInputStream DIStream = new DataInputStream(socket.getInputStream());
                DataOutputStream DOStream = new DataOutputStream(socket.getOutputStream());


                DOStream.write(newCode.getBytes(), 0, newCode.getBytes().length);
                DOStream.flush();

                //  String text = DIStream.readLine();
                DOStream.close();
                DIStream.close();

                socket.close();

                res = "YES";
            } catch (IOException e) {
                res = e.toString();
            }


            return null;
        }


        @Override
        protected void onPostExecute(String result) {
            Toast.makeText(MainActivity.this, res, Toast.LENGTH_LONG).show();

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Toast.makeText(MainActivity.this, res, Toast.LENGTH_LONG);


        }
    }


    private class Cyan extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            final int SocketServerPORT = 3332;
            try {

                socket = new Socket(setIp, SocketServerPORT);

                DataInputStream DIStream = new DataInputStream(socket.getInputStream());
                DataOutputStream DOStream = new DataOutputStream(socket.getOutputStream());


                DOStream.write(getCyan.getBytes(), 0, getCyan.getBytes().length);
                DOStream.flush();

                //  String text = DIStream.readLine();

                DOStream.close();
                DIStream.close();


                socket.close();


            } catch (IOException e) {

            }


            return null;
        }


        @Override
        protected void onPostExecute(String result) {
            Toast.makeText(MainActivity.this, "CYAN", Toast.LENGTH_SHORT).show();
        }

    }


    private class Magenta extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            final int SocketServerPORT = 3332;

            try {

                socket = new Socket(setIp, SocketServerPORT);

                DataInputStream DIStream = new DataInputStream(socket.getInputStream());
                DataOutputStream DOStream = new DataOutputStream(socket.getOutputStream());


                DOStream.write(getMagenta.getBytes(), 0, getMagenta.getBytes().length);
                DOStream.flush();

                //  String text = DIStream.readLine();

                DOStream.close();
                DIStream.close();


                socket.close();


            } catch (IOException e) {

            }


            return null;
        }


        @Override
        protected void onPostExecute(String result) {

            Toast.makeText(MainActivity.this, "MAGENTA", Toast.LENGTH_SHORT).show();
        }

    }


    private class Yellow extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            final int SocketServerPORT = 3332;
            try {

                socket = new Socket(setIp, SocketServerPORT);

                DataInputStream DIStream = new DataInputStream(socket.getInputStream());
                DataOutputStream DOStream = new DataOutputStream(socket.getOutputStream());


                DOStream.write(getYellow.getBytes(), 0, getYellow.getBytes().length);
                DOStream.flush();

                //  String text = DIStream.readLine();

                DOStream.close();
                DIStream.close();


                socket.close();


            } catch (IOException e) {

            }


            return null;
        }


        @Override
        protected void onPostExecute(String result) {

            Toast.makeText(MainActivity.this, "YELLOW", Toast.LENGTH_SHORT).show();
        }

    }


    private void loadContent() {
        String LINK = "http://192.168.6.129:7000/throwdata";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, LINK,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        Log.d("RESULT_NOTIFICATION", response);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                })

        {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("USER", getIp.getText().toString().trim());
                params.put("ID", "ID1,ID2,ID3");

                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        boolean isYes = true;

        //    while (isYes) {
//            String text = "You click at x = " + event.getX() + " and y = " + event.getY();
//            Toast.makeText(this, text, Toast.LENGTH_LONG).show();

        if ((event.getAction() == MotionEvent.ACTION_MOVE)) {
            Log.d("x", event.getX() + "");
            Log.d("y", event.getY() + "");
        }
//            else  if ((event.getAction() == MotionEvent.ACTION_UP)){
//                isYes = false;
//            }


        return true;
    }


}
//
//    @Override
//    public boolean onTouchEvent(MotionEvent ev) {
//        switch (ev.getAction() & MotionEvent.ACTION_MASK) {
//            case MotionEvent.ACTION_DOWN:
//                mDownX = ev.getX();
//                mDownY = ev.getY();
//                isOnClick = true;
//                break;
//            case MotionEvent.ACTION_CANCEL:
//            case MotionEvent.ACTION_UP:
//                if (isOnClick) {
//                    Log.i("ONCLICK", "onClick ");
//                    //TODO onClick code
//                }
//                break;
//            case MotionEvent.ACTION_MOVE:
//                if (isOnClick && (Math.abs(mDownX - ev.getX()) > SCROLL_THRESHOLD || Math.abs(mDownY - ev.getY()) > SCROLL_THRESHOLD)) {
//                    Log.i("ONCLICK", "movement detected");
//                    isOnClick = false;
//                }
//                break;
//            default:
//                break;
//        }
//        return true;
//    }

